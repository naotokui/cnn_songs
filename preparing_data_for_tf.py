# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os
import glob
import time

filenames = glob.glob(os.path.join('/Users/kosukefukui/Qosmo/WASABEAT/music/*.mp3'))

f = open('id_information.txt','w')
time_clock = 0
t = 0

for i in range(len(filenames)):
	start = time.time()

	files = filenames[i].split("/")[6].split(".")[0]
	print files

	cmd = 'curl -XGET "http://api.wasabeat.com/v1/tracks/%s.json"' % files
	data = subprocess.check_output(cmd, shell= True)
	data2 = json.loads(data)

	#binarize
	if data2["genre_id"] in {600,601}:
		binal = 0
	elif data2["genre_id"] in {700,701}:
		binal = 1
	elif data2["genre_id"] in {900,901,902,903,905,907,909}:
		binal = 2
	elif data2["genre_id"] in {1100,1101,1102}:
		binal = 3
	elif data2["genre_id"] in {1300,1301,1302}:
		binal = 4

	for_wirte_txt = "%s,%s,%s,%s,%s,%s,%s" % \
	(data2["id"],\
	data2["artist_id"],\
	data2["genre"]["id"],data2["genre"]["parent_id"],\
	data2["label"]["id"],data2["label"]["area_id"],binal)
	f.write(for_wirte_txt)
	f.write('\n')

	t = t + 1
	time_clock = time_clock + (time.time() - start)

	if t % 100 == 0:
		run_num = (len(filenames) - t) / 100
		elapsed_time = time_clock
		total_time = (run_num * elapsed_time) / 60
		last_time = int(total_time)
		time_clock = 0

		print t
		print "Project finished in %s mins " % last_time

f.close()

