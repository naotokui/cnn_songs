# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os
import glob
import time
import numpy as np

import caffe
import lmdb

#出力数
NUM_CLASSES = 5
#データのframe数
IMAGE_SIZE = 599
#データの全形
IMAGE_PIXELS = IMAGE_SIZE*1*128
train_num = 1500
data_limit = 1700

flatten_data = []
flatten_label = []

# ラベルの整形
idfilepath = '../id_information.txt'
f2 = open(idfilepath)
print "---loading labels----"

def get_genre_id(filename):
	fileid = filename[8:-4]
	for line in f2:
		line = line.rstrip()
		l = line.split(",")
		songid = l[0]
		genreid = l[4]
		if songid == filename[8:-4]:
			return genreid
	return -1

# データの整形
jsondir = '../song_features/*.json'
filenames = glob.glob(os.path.join(jsondir))
filenames = filenames[0:data_limit]
print "----loading data---"
for file_path in filenames:
	json = json.load(open(file_path))
	for key, data in json:
		print key
		genreid = get_genre_id(key)
		if genreid >= 0:
			print genreid
		else:
			print "error no genre info found" 




flatten_label = flatten_label[0:data_limit]

print "データ数 %s" % len(flatten_data)
print "ラベルデータ数 %s" % len(flatten_label)

#訓練データ
train_image = np.asarray(flatten_data[0:train_num], dtype=np.float32)
train_label = np.asarray(flatten_label[0:train_num],dtype=np.float32)

print "訓練データ数 %s" % len(train_image)

#テストデータ
test_image = np.asarray(flatten_data[train_num:data_limit], dtype=np.float32)
test_label = np.asarray(flatten_label[train_num:data_limit],dtype=np.float32)

print "テストデータ数 %s" % len(test_image)


# caffe用に LMDBに書き出し
# http://deepdish.io/2015/04/28/creating-lmdb-in-python/
env = lmdb.open('train_song_lmdb')
with env.begin(write=True) as txn:
    # txn is a Transaction object
    for i in range(train_num):
        datum = caffe.proto.caffe_pb2.Datum()

        datum.channels = train_image.shape[1]
        datum.height = train_image.shape[2]
        datum.width = train_image.shape[3]
        datum.data = train_image[i].tobytes()  # or .tostring() if numpy < 1.9
        datum.label = int(train_label[i])
        str_id = '{:08}'.format(i)

        # The encode is only essential in Python 3
        txn.put(str_id.encode('ascii'), datum.SerializeToString())

env = lmdb.open('test_song_lmdb')
with env.begin(write=True) as txn:
    # txn is a Transaction object
    for i in range(data_limit - rain_num):
        datum = caffe.proto.caffe_pb2.Datum()

        datum.channels = test_image.shape[1]
        datum.height = test_image.shape[2]
        datum.width = test_image.shape[3]
        datum.data = test_image[i].tobytes()  # or .tostring() if numpy < 1.9
        datum.label = int(test_label[i])
        str_id = '{:08}'.format(i)

        # The encode is only essential in Python 3
        txn.put(str_id.encode('ascii'), datum.SerializeToString())
