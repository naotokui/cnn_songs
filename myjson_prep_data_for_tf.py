# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os
import glob
import time
import random

filenames = glob.glob(os.path.join('/Users/kosukefukui/Qosmo/WASABEAT/json/*.json'))

#test用
#filenames = random.sample(filenames,2)

f = open('id_information.txt','w')
time_clock = 0
t = 0
append_id_list = []

#逐次jsonの読み込み
for file_json in filenames:

	data = json.load(open(file_json))
	n_tracks = len(data["tracks"])

	#データにアクセス
	for i in range(n_tracks):
		track_id = data["tracks"][i]["id"]
		artist_id = data["tracks"][i]["artist_id"]
		genre_id = data["tracks"][i]["genre_id"]
		label_id = data["tracks"][i]["label_id"]

		#binarize
		if data["tracks"][i]["genre_id"] in {600,601}:
			binal = 0
		elif data["tracks"][i]["genre_id"] in {700,701}:
			binal = 1
		elif data["tracks"][i]["genre_id"] in {900,901,902,903,905,907,909}:
			binal = 2
		elif data["tracks"][i]["genre_id"] in {1100,1101,1102}:
			binal = 3
		elif data["tracks"][i]["genre_id"] in {1300,1301,1302}:
			binal = 4

		id_tuple = (track_id,artist_id,genre_id,label_id,binal)
		append_id_list.append(id_tuple)

id_list = sorted(append_id_list, key=lambda music: music[0]) 
print "n_itmes %s" % len(id_list)

for m in  range(len(id_list)):
	for_write_txt = "%s,%s,%s,%s,%s" % \
	(str(id_list[m][0]),str(id_list[m][1]),str(id_list[m][2]),str(id_list[m][3]),str(id_list[m][4]))
	f.write(for_write_txt)
	f.write('\n')

f.close()

